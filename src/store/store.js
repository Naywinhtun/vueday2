import Vue from 'vue';
import Vuex from 'vuex';

// import productModule from './store/produtc.js';

Vue.use(Vuex);

const store = new Vuex.Store({
//   namespaced: true,
//   modules: {
//     prods: productModule,
//     cart: cartModule
//   },
  state() {
    return {
      courses: [
        { id: 1, name: "PHP", description: "PHP Laravel course." },
        {
          id: 2,
          name: "JavaScript",
          description: "JavaScript Animation course."
        },
        { id: 3, name: "React", description: "React hook course." }
      ],
      isLoggedIn: false
    };
  },
  mutations: {
    addCourse(state, payload) {
      const name = payload.name;
      const desc = payload.desc;
      const cid = state.courses.length + 1;
      state.courses.push({
        id: cid,
        name: name,
        description: desc
      });
    },
    deleteCourse(state, payload) {
      const deleteId = payload.deleteId;
      state.courses = state.courses.filter(course => course.id !== deleteId);
    },
    login(state, payload) {
      state.isLoggedIn = true;
    },
    logout(state) {
      state.isLoggedIn = false;
    }
  },
  actions: {
    newCourse(context, payload) {
      return context.commit("addCourse", payload);
    },
    removeCourse(context, payload) {
      return context.commit("deleteCourse", payload);
    },
    login(context) {
        return context.commit('login');
    },
    logout(context) {
        return context.commit('logout');
    }
  },
  getters: {
    courses(state) {
      return state.courses;
    },
    isAuth(state) {
      return state.isLoggedIn;
    }
  }
});

export default store;